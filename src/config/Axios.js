import axios from 'axios';

export const instance = axios.create({
  baseURL: 'https://hn.algolia.com/api/v1',
  timeout: 30000,
  headers: {
    'Content-Type': 'application/json'
  }
});
